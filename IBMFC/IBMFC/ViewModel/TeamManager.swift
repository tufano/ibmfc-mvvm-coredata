//
//  TeamManager.swift
//  IBMFC
//
//  Created by Wendell Tufano on 03/02/19.
//  Copyright © 2019 Wendell Tufano. All rights reserved.
//

import Foundation
import CoreData

///
///Manager teams from CoreData
///

class TeamManager {
    
    static let shared = TeamManager()
    var teams:[TeamMO] = []
    
    func fetchTeams(with context: NSManagedObjectContext){
        
        let fetchRequest:NSFetchRequest<TeamMO> = TeamMO.fetchRequest()
        let sortByName: NSSortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        
        fetchRequest.sortDescriptors = [sortByName]
        
        do{
            try teams = context.fetch(fetchRequest)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func deleteTeam(index:Int, context: NSManagedObjectContext){
        
        let team = teams[index]
        
        context.delete(team)
        
        do {
            try context.save()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func createTeamMO(context:NSManagedObjectContext, team:TeamJSON) -> TeamMO{
        let teamMO = TeamMO(context: context)
        teamMO.name = team.name
        teamMO.teamId = Int32(team.teamId)
        
        //Default score for all teams
        teamMO.goals = 0
        teamMO.draw = 0
        teamMO.victory = 0
        teamMO.lose = 0
        
        do{
            try context.save()
        }catch {
            print(error.localizedDescription)
        }
        
        return teamMO
    }
    
    private init(){
        
    }
    
}
