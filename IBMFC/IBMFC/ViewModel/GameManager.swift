//
//  GameManager.swift
//  IBMFC
//
//  Created by Wendell Tufano on 03/02/19.
//  Copyright © 2019 Wendell Tufano. All rights reserved.
//

import Foundation
import CoreData

///
/// Manager games from CoreData
///

class GameManager {
    
    static let shared = GameManager()
    var games:[GameMO] = []
    
    func fetchGames(with context:NSManagedObjectContext){
        
        let fetchRequest:NSFetchRequest<GameMO> = GameMO.fetchRequest()
        let sortByGameId:NSSortDescriptor = NSSortDescriptor(key: "gameId", ascending: true)
        
        fetchRequest.sortDescriptors = [sortByGameId]
        
        do {
            try games = context.fetch(fetchRequest)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func createGameMO(context:NSManagedObjectContext, gameId:Int?, teamHomeId:Int, teamAwayId:Int, scoreHomeId:Int, scoreAwayId:Int) -> GameMO {
        
        var currentId:Int32 = 0
        
        if gameId == nil {
            
            fetchGames(with: context)
            
            //retrive gameId + 1 from las gameId on context
            let gameId = games.last?.gameId ?? 0
            currentId = gameId + 1
        }
        
        let newGame = GameMO(context: context)
        newGame.gameId = currentId
        newGame.teamHomeId = Int32(teamHomeId)
        newGame.teamAwayId = Int32(teamAwayId)
        newGame.scoreHomeId = Int32(scoreHomeId)
        newGame.scoreAwayId = Int32(scoreAwayId)
        
        do {
            try context.save()
            games.append(newGame)
        } catch {
            print(error.localizedDescription)
        }
        
        return newGame
    }
    
    func createGameMOWithArray(context:NSManagedObjectContext, with games:[GameJSON]){
        
        for game in games{
            
            let newGame = GameMO(context: context)
            newGame.gameId = Int32(game.gameId)
            newGame.teamHomeId = Int32(game.teamHomeId)
            newGame.teamAwayId = Int32(game.teamAwayId)
            newGame.scoreHomeId = Int32(game.scoreHomeId)
            newGame.scoreAwayId = Int32(game.scoreAwayId)
            
            do {
                try context.save()
                self.games.append(newGame)
            } catch {
                print(error.localizedDescription)
            }
            
        }
        
    }
    
    private init(){}
    
}
