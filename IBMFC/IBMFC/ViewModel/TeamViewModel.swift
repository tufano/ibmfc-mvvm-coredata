//
//  TeamViewModel.swift
//  IBMFC
//
//  Created by Wendell Tufano on 01/02/19.
//  Copyright © 2019 Wendell Tufano. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class TeamViewModel{
    
    var teamId:Int
    var name:String
    var victory:Int = 0
    var lose:Int = 0
    var draw:Int = 0
    var goals:Int = 0
    var points:Int = 0
    
    //Dependecy Injection (DI)
    init(team:TeamMO) {

        self.teamId = Int(team.teamId)
        self.name = team.name
        self.victory += Int(team.victory)
        self.lose += Int(team.lose)
        self.draw += Int(team.draw)
        self.goals += Int(team.goals)
        self.points += Int(team.points)

    }
    
}
