//
//  StrikeViewModel.swift
//  IBMFC
//
//  Created by Wendell Tufano on 01/02/19.
//  Copyright © 2019 Wendell Tufano. All rights reserved.
//

import Foundation
import UIKit

class StrikerViewModel{
    
    var strikers = [StrikerJSON]()
    
    //Dependecy Injection (DI)
    init(strikers:[StrikerJSON]) {
        self.strikers = strikers
    }
    
}
