//
//  GameViewModel.swift
//  IBMFC
//
//  Created by Wendell Tufano on 01/02/19.
//  Copyright © 2019 Wendell Tufano. All rights reserved.
//

import Foundation
import UIKit

class GameViewModel {
  
    var games:[GameMO]?
    
    //Dependecy Injection (DI)
    init(games:[GameMO]) {
        self.games = games
    }
    
    func generateScore(teams:[TeamViewModel], newGame: GameMO) -> [TeamViewModel]{

        let homeId = Int(newGame.teamHomeId - 1)
        let awayId = Int(newGame.teamAwayId - 1)
        
        //if home Win
        if newGame.scoreHomeId > newGame.scoreAwayId {
            teams[homeId].victory += 1
            teams[homeId].goals += Int(newGame.scoreHomeId)
            teams[homeId].points += 3
            
            teams[awayId].lose += 1
            teams[awayId].goals += Int(newGame.scoreAwayId)
        }
        //if away Win
        if newGame.scoreAwayId > newGame.scoreHomeId {
            teams[awayId].victory += 1
            teams[awayId].goals += Int(newGame.scoreAwayId)
            teams[awayId].points += 3
            
            teams[homeId].lose += 1
            teams[homeId].goals += Int(newGame.scoreHomeId)
        }
        //if draw
        if newGame.scoreHomeId == newGame.scoreAwayId {
            teams[homeId].draw += 1
            teams[homeId].goals += Int(newGame.scoreHomeId)
            teams[homeId].points += 1
            
            teams[awayId].draw += 1
            teams[awayId].goals += Int(newGame.scoreAwayId)
            teams[awayId].points += 1
        }
        
        self.games?.append(newGame)
        return teams
        
    }
    
    
    func generateStaticScore(teams:[TeamViewModel]) -> [TeamViewModel]{
        
        guard let games = self.games else { return teams }
        
        for game in games{
            let homeId = Int(game.teamHomeId - 1)
            let awayId = Int(game.teamAwayId - 1)
            
            //if home Win
            if game.scoreHomeId > game.scoreAwayId {
                teams[homeId].victory += 1
                teams[homeId].goals += Int(game.scoreHomeId)
                teams[homeId].points += 3
                
                teams[awayId].lose += 1
                teams[awayId].goals += Int(game.scoreAwayId)
            }
            //if away Win
            if game.scoreAwayId > game.scoreHomeId {
                teams[awayId].victory += 1
                teams[awayId].goals += Int(game.scoreAwayId)
                teams[awayId].points += 3
                
                teams[homeId].lose += 1
                teams[homeId].goals += Int(game.scoreHomeId)
            }
            //if draw
            if game.scoreHomeId == game.scoreAwayId {
                teams[homeId].draw += 1
                teams[homeId].goals += Int(game.scoreHomeId)
                teams[homeId].points += 1
                
                teams[awayId].draw += 1
                teams[awayId].goals += Int(game.scoreAwayId)
                teams[awayId].points += 1
            }
        }
        return teams
        
    }
    
}
