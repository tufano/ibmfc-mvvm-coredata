//
//  TeamMO+CoreDataProperties.swift
//  IBMFC
//
//  Created by Wendell Tufano on 02/02/19.
//  Copyright © 2019 Wendell Tufano. All rights reserved.
//
//

import Foundation
import CoreData


extension TeamMO {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TeamMO> {
        return NSFetchRequest<TeamMO>(entityName: "Team")
    }

    @NSManaged public var draw: Int32
    @NSManaged public var goals: Int32
    @NSManaged public var lose: Int32
    @NSManaged public var name: String
    @NSManaged public var points: Int32
    @NSManaged public var teamId: Int32
    @NSManaged public var victory: Int32

}
