//
//  ViewController+CoreData.swift
//  IBMFC
//
//  Created by Wendell Tufano on 02/02/19.
//  Copyright © 2019 Wendell Tufano. All rights reserved.
//

import Foundation
import UIKit
import CoreData

extension UIViewController {
    
    var context:NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
}
