//
//  GameMO+CoreDataProperties.swift
//  IBMFC
//
//  Created by Wendell Tufano on 02/02/19.
//  Copyright © 2019 Wendell Tufano. All rights reserved.
//
//

import Foundation
import CoreData


extension GameMO {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<GameMO> {
        return NSFetchRequest<GameMO>(entityName: "Game")
    }

    @NSManaged public var gameId: Int32
    @NSManaged public var scoreAwayId: Int32
    @NSManaged public var scoreHomeId: Int32
    @NSManaged public var teamAwayId: Int32
    @NSManaged public var teamHomeId: Int32
    

}

