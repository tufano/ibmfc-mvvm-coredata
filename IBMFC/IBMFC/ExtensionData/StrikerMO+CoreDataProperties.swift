//
//  StrikerMO+CoreDataProperties.swift
//  IBMFC
//
//  Created by Wendell Tufano on 02/02/19.
//  Copyright © 2019 Wendell Tufano. All rights reserved.
//
//

import Foundation
import CoreData


extension StrikerMO {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<StrikerMO> {
        return NSFetchRequest<StrikerMO>(entityName: "Striker")
    }

    @NSManaged public var goals: Int16
    @NSManaged public var playerId: Int16
    @NSManaged public var playerName: String?
    @NSManaged public var playerPhotoURL: String?
    @NSManaged public var teamId: Int16

}
