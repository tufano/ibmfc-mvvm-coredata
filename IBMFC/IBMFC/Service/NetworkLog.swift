//
//  HUBHTTPRequester+Log.swift
//  HUBConnectorExample
//
//  Created by Lucas Torquato on 5/07/17.
//  Copyright © 2017 IBM Brasil. All rights reserved.
//

import Foundation
import Alamofire

public typealias JSON = [String: Any]

class NetworkLog: NSObject {
    
    fileprivate class func show(_ message: String) {
        print(message)
    }
    
    class func logDivider() {
        show("---------------------")
    }
    
    class func logError(_ error: NSError?) {
        logDivider()
        
        if let error = error {
            show("⚠️ Error: \(error.localizedDescription)")
            
            if let reason = error.localizedFailureReason {
                show("Reason: \(reason)")
            }
            
            if let suggestion = error.localizedRecoverySuggestion {
                show("Suggestion: \(suggestion)")
            }
        } else {
            show("⚠️ Error: nil")
        }
    }
    
    class func logRequest(_ request: URLRequest) {
        logDivider()
        
        if let url = request.url?.absoluteString {
            show("📤 Request: \(request.httpMethod!) \(url)")
        }
        
        if let headers = request.allHTTPHeaderFields {
            self.logHeaders(headers as [String: AnyObject])
        }
        
        if let httpBody = request.httpBody {
            do {
                let json = try JSONSerialization.jsonObject(with: httpBody, options: .allowFragments)
                let pretty = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                
                if let string = NSString(data: pretty, encoding: String.Encoding.utf8.rawValue) {
                    show("JSON: \(string)")
                }
            } catch {
                if let string = NSString(data: httpBody, encoding: String.Encoding.utf8.rawValue) {
                    show("Data: \(string)")
                }
            }
            
        }
        
    }
    
    class func logResponse(_ response: URLResponse?, data: Data? = nil) {
        logDivider()
        
        if let response = response {
            
            if let url = response.url?.absoluteString {
                show("📥 Response: \(url)")
            }
            
            if let httpResponse = response as? HTTPURLResponse {
                let localisedStatus = HTTPURLResponse.localizedString(forStatusCode: httpResponse.statusCode).capitalized
                show("Status: \(httpResponse.statusCode) - \(localisedStatus)")
            }
            
            if let headers = (response as? HTTPURLResponse)?.allHeaderFields as? [String: AnyObject] {
                self.logHeaders(headers)
            }
            
            guard let data = data else { return }
            
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                if let theJson = json as? JSON {
                    let pretty = try JSONSerialization.data(withJSONObject: theJson, options: .prettyPrinted)
                    if let string = NSString(data: pretty, encoding: String.Encoding.utf8.rawValue) {
                        show("JSON: \(string)")
                    }
                } else {
                    if let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                        show("Data: \(string)")
                    }
                }
                
            } catch {
                if let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                    show("Data: \(string)")
                }
            }
            
        } else {
            show("📥 Response: nil ")
        }
        
    }
    
    class func logCacheReponse(_ request: URLRequest?, json: JSON) {
        logDivider()
        
        if let request = request {
            
            if let url = request.url?.absoluteString {
                show("📥 Cache Response: \(url)")
            }
            
            show("Status: CACHE")
            
            do {
                let pretty = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                if let string = NSString(data: pretty, encoding: String.Encoding.utf8.rawValue) {
                    show("JSON: \(string)")
                }
            } catch {
                show("Error to print JSON")
            }
            
        } else {
            show("📥 Response: nil ")
        }
        
    }
    
    class func logHeaders(_ headers: [String: AnyObject]) {
        show("Headers: [")
        for (key, value) in headers {
            show("  \(key) : \(value)")
        }
        show("]")
    }
    
    class func logValue<Value>(_ value: Value) {
        show("📦 Value from Response:")
        show("\(value)")
    }
    
    class func logSuccess() {
        show("📦 Success!")
    }
    
}

