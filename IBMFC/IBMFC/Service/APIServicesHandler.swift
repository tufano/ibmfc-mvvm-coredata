//
//  GameService.swift
//  IBMFC
//
//  Created by Wendell Tufano on 26/12/18.
//  Copyright © 2018 Wendell Tufano. All rights reserved.
//

import Foundation
import Alamofire
import SwiftKeychainWrapper


///    Handler to web services.
///
///    get: https://private-anon-6f31fd0848-ibmfc.apiary-mock.com/games
///    get: https://private-anon-92d7660e4e-ibmfc.apiary-mock.com/login
///    get: https://private-anon-6f31fd0848-ibmfc.apiary-mock.com/strikers

class GameRequest {
    
   static func request(onComplete: @escaping ([GameJSON]?)->Void) {
        var gamesJSON:[GameJSON] = []
    
        Alamofire.request("https://private-anon-6f31fd0848-ibmfc.apiary-mock.com/games").responseJSON { (response) in
            guard let data = response.data,
                let gameData = try? JSONDecoder().decode(GameData.self, from: data) else{ return onComplete(nil)}
        
            gamesJSON = gameData.data
            onComplete(gamesJSON)
        } 
    }
}

struct LoginRequest {
    
    static func request(bodyRequest:Parameters, onComplete: @escaping(Bool?)->Void){
        let request = Alamofire.request("https://private-anon-6f31fd0848-ibmfc.apiary-mock.com/login", method:.post, parameters:bodyRequest, encoding: JSONEncoding.default)
        
        if let requestURL = request.request{
            NetworkLog.logRequest(requestURL)
        }
        
        request.responseJSON { (response) in
            NetworkLog.logResponse(response.response)
            guard let data = response.data,
                let autheticatorResponse = try? JSONDecoder().decode(Info.self, from: data) else { return }
            
                //Return API request
                let acessToken = autheticatorResponse.data.token
                let userAcess = autheticatorResponse.success
            
                //Save acess in keyChain
                KeychainWrapper.standard.set(acessToken, forKey: "acessToken")
                KeychainWrapper.standard.set(userAcess, forKey: "userAccess")
            
            DispatchQueue.main.async {
                onComplete(userAcess)
            }
        }
    }
}

struct TeamRequest {
    
    static func request() -> [TeamJSON] {
        var teams:[TeamJSON] = []
        
        let fileURL = Bundle.main.url(forResource: "Teams.json", withExtension: nil)
        guard let file = fileURL,
            let data = try? Data(contentsOf: file) else {
                return teams
        }
        do{
            let dataTeams = try JSONDecoder().decode(DataTeam.self, from: data)
            teams.append(contentsOf: dataTeams.teams)
            return teams
        }catch{
            print(error.localizedDescription)
        }
        return teams
    }
}

struct StrikesRequest{
    
    static func request(onComplete: @escaping ([StrikerJSON]?)-> Void){
        let request = Alamofire.request("https://private-anon-6f31fd0848-ibmfc.apiary-mock.com/strikers")
        if let requestURL = request.request{
            NetworkLog.logRequest(requestURL)
        }
        request.responseJSON { (response) in
            NetworkLog.logResponse(response.response)
            guard let data = response.data,
                let strikerJSON = try? JSONDecoder().decode(StrikersData.self, from: data) else { return }
            DispatchQueue.main.async {
               onComplete(strikerJSON.data)
            }
        }
    }
}
