//
//  LoginViewController.swift
//  IBMFC
//
//  Created by Wendell Tufano on 29/08/18.
//  Copyright © 2018 Wendell Tufano. All rights reserved.
//

import UIKit
import Alamofire
import SwiftKeychainWrapper

class LoginViewController: UIViewController {
    
    //MARK: - PROPERTIES
    var saveAcessToken:Bool = false
    var saveUserAcess:Bool = false
    var isLoggin:Bool? = false

    //MARK: - COMPONENTS
    @IBOutlet weak var tfLogin: CustomUITextField!
    @IBOutlet weak var tfPassword: CustomUITextField!
    @IBOutlet weak var loadState: UIActivityIndicatorView!
    @IBOutlet weak var btLogin: UIButton!
    
    //MARK: - LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: - BUTTON ACTION
    @IBAction func loginAuthentication(_ sender: Any) {
        loginAuthenticator()
    }
    
    @IBAction func didUnwindFromHome(_ sender: UIStoryboardSegue){
        let _ = KeychainWrapper.standard.removeAllKeys()
        tfPassword.text = nil
    }
    
    //MARK: - FUNC
    func loginAuthenticator(){
        
        if tfLogin.text!.isEmpty && tfPassword.text!.isEmpty {
            
            // create the alert
            let alert = UIAlertController(title: "Campo(s) Vazio(s)", message: "Favor preencher todos os campos.", preferredStyle: .alert)
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            // show the alert
            self.present(alert, animated: true, completion: nil)
            return
            
        } else {
            
            guard let user = tfLogin.text else {return}
            guard let pass = tfPassword.text else {return}
            
            //start loadig state
            prepareLoadingState()
            
            //loadState.
            validateLogin(user: user, pass: pass)
            
        }
    } 
    
    func prepareLoadingState(){
        loadState.startAnimating()
        btLogin.titleLabel?.text = ""
        btLogin.alpha = 0.9
        tfLogin.isUserInteractionEnabled = false
        tfPassword.isUserInteractionEnabled = false
    }
    
    func didFinishedLoadingState(){
        loadState.stopAnimating()
        btLogin.titleLabel?.text = "Entrar"
        btLogin.alpha = 1
        tfLogin.isUserInteractionEnabled = true
        tfPassword.isUserInteractionEnabled = true
    }
    
    func validateLogin(user: String, pass:String){
        //body request
        let parameters:Parameters = ["username": "\(user)","password": "\(pass)"]
        LoginRequest.request(bodyRequest: parameters) { (result) in
            let isLogging = result ?? false
            if isLogging {
                self.loginDone()
            }
        }
    }
    
    func loginDone(){
        //Next Screen
        performSegue(withIdentifier: "indexSegue", sender: nil)
        didFinishedLoadingState()
    }
}

//MARK: - EXTENSION VIEWCONTROLLER
extension LoginViewController{
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
}
