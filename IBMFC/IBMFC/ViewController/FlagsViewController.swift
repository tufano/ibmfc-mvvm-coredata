//
//  FlagsViewController.swift
//  IBMFC
//
//  Created by Wendell Tufano on 30/08/18.
//  Copyright © 2018 Wendell Tufano. All rights reserved.
//

import UIKit

class FlagsViewController: UIViewController {

    //MARK: - PROPERTIES
    var teamManager = TeamManager.shared
    
    var isSearching = false
    var flagName:String? = nil
    var flagId:Int? = nil

    var filteredTeamViewModels = [TeamViewModel]()
    var teamViewModel = [TeamViewModel]()
    
    
    //MARK: - OUTLETS
    @IBOutlet weak var tbView: UITableView!
    @IBOutlet weak var exitButton: UIBarButtonItem!
    @IBOutlet weak var addButton: UIBarButtonItem!
    
    //MARK: - LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        loadTeams()
    }
    
    //MARK: - FUNC
    func loadTeams(){
        
        //fetch from dataBase
        teamManager.fetchTeams(with: context)        
        teamViewModel = teamManager.teams.map({return TeamViewModel(team: $0)})
        
    }
    
    //MARK: - BUTTONS ACTION
    @IBAction func exit(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}

//MARK: - TABLEVIEW DELEGATE PROTOCOL
extension FlagsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching {
            return filteredTeamViewModels.count
        }
        return teamViewModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "flagCell", for: indexPath) as! FlagTableViewCell
        if isSearching {
            let team = filteredTeamViewModels[indexPath.row]
            cell.prepareFlagCell(with: team)
        } else {
            let team = teamViewModel[indexPath.row]
            cell.prepareFlagCell(with: team)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //tableVieW.cellForRow(at: indexPath)?.accessoryType = UITableViewCell.AccessoryType.checkmark
        if isSearching {
            self.flagName = filteredTeamViewModels[indexPath.row].name
            self.flagId = filteredTeamViewModels[indexPath.row].teamId
        } else {
            self.flagName = teamViewModel[indexPath.row].name
            self.flagId = teamViewModel[indexPath.row].teamId
        }
    }
}

//MARK: - SEARCHBAR DELEGATE
extension FlagsViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let formatedText = searchText.replacingOccurrences(of: " ", with: "_")
        filteredTeamViewModels = teamViewModel.filter({$0.name.prefix(formatedText.count) == formatedText})
        isSearching = true
        tbView.reloadData()
    }
}

