//
//  HomeViewController.swift
//  IBMFC
//
//  Created by Wendell Tufano on 30/08/18.
//  Copyright © 2018 Wendell Tufano. All rights reserved.
//

import UIKit
import Alamofire

class HomeViewController: UIViewController {
    
    //MARK: - PROPERTIES
    var teamsManager = TeamManager.shared
    var gamesManager = GameManager.shared
    
    //ViewModel
    var gameViewModel:GameViewModel?
    
    var strikerViewModel:StrikerViewModel? {
        didSet{
            strikerCollectionView.reloadData()
        }
    }
    
    var teamViewModel = [TeamViewModel](){
        didSet{
            resultTableView.reloadData()
        }
    }
    
    //MARK: - COMPONENTS
    @IBOutlet weak var resultTableView: UITableView!
    @IBOutlet weak var strikerCollectionView: UICollectionView!

    //MARK: - LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        teamsManager.fetchTeams(with: context)
        gamesManager.fetchGames(with: context)

        loadStaticTeams()
        loadStaticScore()
        loadStrikers()
    }
    
    //MARK: - BUTTON EXIT
    @IBAction func didUnwindFromGames(_ sender: UIStoryboardSegue){
        
        guard let gameVC = sender.source as? GamesViewController else { return }
        let searchHomeTeam = teamViewModel.filter({$0.name == gameVC.homeName?.replacingOccurrences(of: " ", with: "_")}).first
        let searchAwayTeam = teamViewModel.filter({$0.name == gameVC.awayName?.replacingOccurrences(of: " ", with: "_")}).first
        
        if searchHomeTeam == nil || searchAwayTeam == nil{
            return
        }
        
        //Result from GamesViewController
        guard let textHome = gameVC.textHomeGoal.text else { return }
        guard let textAway = gameVC.textAwayGoal.text else { return }
        guard let scoreHome = Int(textHome) else {return}
        guard let scoreAway = Int(textAway) else {return}
        
        //New object gameViewModel
        let newGame = gamesManager.createGameMO(context: context, gameId: nil, teamHomeId: searchHomeTeam!.teamId, teamAwayId: searchAwayTeam!.teamId, scoreHomeId: scoreHome, scoreAwayId: scoreAway)
        
        //Create new score
        self.teamViewModel = gameViewModel?.generateScore(teams: teamViewModel, newGame: newGame) ?? []
    }
    
    //MARK: - FUNC
    func loadStaticScore(){
        
        if !(gamesManager.games.isEmpty) {
            let games = gamesManager.games
            self.gameViewModel = GameViewModel(games: games)
            self.teamViewModel = self.gameViewModel!.generateStaticScore(teams: self.teamViewModel)
            
            return
        }
        
        GameRequest.request { (result) in
            if let games = result {
                self.gamesManager.createGameMOWithArray(context: self.context, with: games)
                self.gameViewModel = GameViewModel(games: self.gamesManager.games)
                self.teamViewModel = self.gameViewModel!.generateStaticScore(teams: self.teamViewModel)
            }
        }
    }
    
    //load strikers from handler web services.
    func loadStrikers(){
        StrikesRequest.request(onComplete: { (result) in
            if let strikers = result {
                self.strikerViewModel = StrikerViewModel(strikers: strikers)
            }
        })
    }
    
    //load strikers from handler web services.
    func loadStaticTeams(){
        
        if !(teamsManager.teams.isEmpty) {
            for teamMO in teamsManager.teams {
                let loadedTeam = TeamViewModel(team: teamMO)
                teamViewModel.append(loadedTeam)
            }
            return
        }
        
        let staticTeams = TeamRequest.request()
        for staticTeam in staticTeams {
            let teamMO = teamsManager.createTeamMO(context: context, team: staticTeam)
            let loadedTeam = TeamViewModel(team: teamMO)
            teamViewModel.append(loadedTeam)
        }
    }
}

//MARK: - EXTENSION UITABLEVIEW PROTOCOLS
extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return teamViewModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "teamCell", for: indexPath) as! TeamTableViewCell
        
        //Sorted Array
        let sortedArray = teamViewModel.sorted(by:{if $0.points == $1.points{
                   return $0.goals > $1.goals
                }
            return $0.points > $1.points
        })
            
        //Custom Cell
        let team = sortedArray[indexPath.row]
        cell.prepareTeamCell(with: team)
        return cell
        
    }
    
}

//MARK: - EXTENSION UICOLLECTIONVIEW PROTOCOLS
extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        guard let striker = strikerViewModel?.strikers else {
            return 0
        }
        
        return striker.count

    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "strikerCollectionItem", for: indexPath) as! StrikerCollectionViewCell

        guard let strikers = strikerViewModel?.strikers else {
            return cell
        }

        //Sorted Array
        let sortedArray = strikers.sorted(by:{$0.goals > $1.goals})

        //Custom Cell
        let striker = sortedArray[indexPath.row]
            let position = sortedArray.firstIndex(where: {$0 === striker})
                let teamName = teamViewModel[striker.teamId - 1].name
        
        if let position = position {
            let positionStrike = position + 1
            cell.prepareStriekrCell(with: striker, flagName: teamName, positionNumber: positionStrike)
        }
        return cell
    }
    
}
