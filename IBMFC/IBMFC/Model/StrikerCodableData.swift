//
//  StrikerJSON.swift
//  IBMFC
//
//  Created by Wendell Tufano on 30/08/18.
//  Copyright © 2018 Wendell Tufano. All rights reserved.
//

import Foundation

class StrikersData:Codable{
    let status:String
    let data:[StrikerJSON]
}

class StrikerJSON:Codable {
    let playerId:Int
    let playerName:String
    let goals:Int
    let teamId:Int
    let playerPhotoURL:String
}
