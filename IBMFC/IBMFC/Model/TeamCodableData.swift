//
//  TeamJSON.swift
//  IBMFC
//
//  Created by Wendell Tufano on 30/08/18.
//  Copyright © 2018 Wendell Tufano. All rights reserved.
//

import Foundation

struct DataTeam:Codable {
    let teams:[TeamJSON]
}

struct TeamJSON:Codable {
    let teamId:Int
    let name:String
}

