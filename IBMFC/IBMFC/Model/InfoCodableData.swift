//
//  DataAPI.swift
//  IBMFC
//
//  Created by Wendell Tufano on 30/08/18.
//  Copyright © 2018 Wendell Tufano. All rights reserved.
//

import Foundation

struct Info:Codable {
    let success: Bool
    let data:DataAPI
}

struct DataAPI:Codable {
    let email:String
    let token:String
}

